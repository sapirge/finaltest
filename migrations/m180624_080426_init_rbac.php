<?php

use yii\db\Migration;

/**
 * Class m180624_080426_init_rbac
 */
class m180624_080426_init_rbac extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        
             $auth = Yii::$app->authManager;
            
            // add "author" role and give this role the "createPost" permission
            $manager = $auth->createRole('manager');
            $auth->add($manager);
    
            $employee = $auth->createRole('employee');
            $auth->add($employee);
            
            $auth->addChild($manager, $employee);
        
    
            $createUser  = $auth->createPermission('createUser');
            $auth->add($createUser);
    
            $updateUser  = $auth->createPermission('updateUser');
            $auth->add($updateUser);

            $updateOwnUser  = $auth->createPermission('updateOwnUser');
            $auth->add($updateOwnUser);
    
            $viewUsers  = $auth->createPermission('viewUsers');
            $auth->add($viewUsers);
    
            $createTask  = $auth->createPermission('createTask');
            $auth->add($createTask);      

            $updateTask  = $auth->createPermission('updateTask');
            $auth->add($updateTask );  

            $deleteTask = $auth->createPermission('deleteTask');
            $auth->add($deleteTask);  

             $viewTask  = $auth->createPermission('viewTask');
            $auth->add($viewTask);  

            $viewTaskList  = $auth->createPermission('viewTaskList');
            $auth->add($viewTaskList);

            $viewOwnUser = $auth->createPermission('viewOwnUser');
    
            $rule = new \app\rbac\EmployeeRule;
            $auth->add($rule);
            
            $viewOwnUser->ruleName = $rule->name;                 
            $auth->add($viewOwnUser);                                                    
            
            $auth->addChild($employee, $createTask);
            $auth->addChild($employee, $viewOwnUser);
            $auth->addChild($employee, $updateOwnUser);
            $auth->addChild($employee, $viewTask);
            $auth->addChild($employee, $viewTaskList);
            $auth->addChild($employee, $viewUsers); 
            $auth->addChild($manager, $updateTask); 
            $auth->addChild($manager, $updateUser);
            $auth->addChild($manager, $deleteTask);  
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180624_080426_init_rbac cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180624_080426_init_rbac cannot be reverted.\n";

        return false;
    }
    */
}
